//
//  Guesser.swift
//  Guesser
//
//  Created by Sai Sri Lakshmi on 27/02/19.
//  Copyright © 2019 Sai Sri Lakshmi. All rights reserved.
//


import Foundation

class Guesser{
    
    static var shared = Guesser()
    
    private var correctAnswer:Int
    private var _numAttempts:Int
    private var guesses:[Guess]
    var numAttempts:Int{
        return _numAttempts
    }
    private convenience init(){
        let random = Int.random(in: 1...10)
        self.init(correctAnswer: random,_numAttempts: 0,guesses: [])
    }
    
    init(correctAnswer:Int, _numAttempts:Int,guesses:[Guess]){
        self.correctAnswer = correctAnswer
        self._numAttempts = _numAttempts
        self.guesses = guesses
    }
    
    func createNewProblem(){
        correctAnswer = Int.random(in: 1...10)
        _numAttempts = 0
    }
    func amIRight(guess:Int) -> String{
        _numAttempts = _numAttempts + 1
        if guess < correctAnswer{
            return Result.tooLow.rawValue
        }else if guess > correctAnswer{
            return Result.tooHigh.rawValue
        }else{
            guesses.append(Guess(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
            return Result.correct.rawValue
        }
    }
    
    func guess(index:Int) ->Guess {
        return guesses[index]
    }
    
    func numGuesses()->Int{
        return guesses.count
    }
    
    func clearStatistics(){
        guesses = []
    }
    
    func minimumNumAttempts() ->Int{
        
        if guesses.isEmpty{
            return 0
        }else{
            var minimum = guesses[0].numAttemptsRequired
            for i in guesses{
                if minimum > i.numAttemptsRequired{
                    minimum = i.numAttemptsRequired
                }
            }
            
            return minimum
        }
        
    }
    
    func  maximumNumAttempts() ->Int{
        
        if guesses.isEmpty{
            return 0
        }else{
            var maximum = guesses[0].numAttemptsRequired
            for i in guesses{
                if maximum < i.numAttemptsRequired{
                    maximum = i.numAttemptsRequired
                }
            }
            return maximum
        }
    }
    
    func mean() ->Double{
        if guesses.isEmpty{
            return 0
        }else{
            var mean = 0.0
            var sum = 0.0
            for i in guesses{
                sum = sum + Double(i.numAttemptsRequired)
            }
            mean = sum/Double(numGuesses())
            return mean
        }
    }
    
    func stdDiv() -> Double{
        if guesses.isEmpty{
            return 0
        }else{
            var temp = 0.0
            var stDiv = 0.0
            for i in guesses{
                let temp1 = pow(Double(i.numAttemptsRequired)-mean(),2)
                temp = temp + temp1
            }
            stDiv = sqrt(temp/Double(numGuesses()))
            return stDiv
        }
    }
    
}

struct Guess {
    var correctAnswer:Int
    var numAttemptsRequired:Int
}

enum Result:String {case tooLow = "Too Low", tooHigh = "Too High", correct = "Correct"}

