//
//  SecondViewController.swift
//  Guesser
//
//  Created by Sai Sri Lakshmi on 27/02/19.
//  Copyright © 2019 Sai Sri Lakshmi. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Guesser.shared.numGuesses()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = tableView.dequeueReusableCell(withIdentifier: "history")!
        data.textLabel?.text = String("Correct Answer :\(Guesser.shared.guess(index:indexPath.row).correctAnswer)")
        data.detailTextLabel?.text = String("# Attempts: \(Guesser.shared.guess(index: indexPath.row).numAttemptsRequired)")
            return data
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.loadView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

}

