//
//  ThirdViewController.swift
//  Guesser
//
//  Created by Sai Sri Lakshmi on 27/02/19.
//  Copyright © 2019 Sai Sri Lakshmi. All rights reserved.
//


import UIKit

class ThirdViewController: UIViewController {
    
    
    @IBOutlet weak var minLBL: UILabel!
    
    @IBOutlet weak var maxLBL: UILabel!
    
    @IBOutlet weak var meanLBL: UILabel!
    
    
    @IBOutlet weak var stdDevLBL: UILabel!
    
    @IBAction func clearStatisticsLBL(_ sender: Any) {
        minLBL.text = "0"
        maxLBL.text = "0"
        meanLBL.text = "0.0"
        stdDevLBL.text = "0.0"
        Guesser.shared.clearStatistics()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        minLBL.text = String(Guesser.shared.minimumNumAttempts())
        maxLBL.text = String(Guesser.shared.maximumNumAttempts())
        meanLBL.text = String(format: "%.1f",Guesser.shared.mean())
        stdDevLBL.text = String(format: "%.1f",Guesser.shared.stdDiv())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewDidLoad()
    }
    
    
   
}
