//
//  FirstViewController.swift
//  Guesser
//
//  Created by Sai Sri Lakshmi on 27/02/19.
//  Copyright © 2019 Sai Sri Lakshmi. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var guessTF: UITextField!
    
    @IBOutlet weak var outputLBL: UILabel!
    
    @IBAction func amIRightBTN(_ sender: Any) {
        let num_guessed = Int(guessTF.text!)
        if num_guessed == nil {
            
        }
        else{
            let output_msg = Guesser.shared.amIRight(guess: num_guessed!)
            if output_msg == "Too Low" || output_msg == "Too High" {
                outputLBL.text = output_msg
            }
            else {
                displayMessage()
                
                Guesser.shared.createNewProblem()
                clear()
            }
        }
    }
    
    @IBAction func createNewProblemBTN(_ sender: Any) {
        Guesser.shared.createNewProblem()

    }
    func displayMessage(){
        let alert_msg = UIAlertController(title: "Well done",message: "You got it in \(Guesser.shared.numAttempts) tries",preferredStyle: .alert)
        alert_msg.addAction(UIAlertAction(title: "OK", style: .default,handler: nil))
        self.present(alert_msg, animated: true, completion: nil)
    }
    
    func clear(){
        outputLBL.text = ""
        guessTF.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

